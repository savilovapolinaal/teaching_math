from task import check, Feedback

TEST_CASES = [
    # reply, score, feedback
    ('34 20 -16\n-22 4 20\n-26 -18 12', 1, Feedback.CORRECT),
    ('-50 -40 8\n-4 -8 -10\n46 36 -6', 0, Feedback.INCORRECT_COMM_WITHOUT_TR),
    ('25 40 -7\n-15 8 13\n-18 -36 4', 0, Feedback.INCORRECT_COMM_INCORRECT_TR),
    ('1 2 3 4\n5 6 7 1', 0, Feedback.INCORRECT_COMM_WRONG_DIM),
    ('1 2 3\n4 5 6\n7 8 9', 0, Feedback.INCORRECT_COMM_OTHER),
    ('34 21 -16\n-22 4 20\n-5 -18 12', 0, Feedback.INCORRECT_COMM_TWO_WRONG_EL),
    ('34 20 -16\n-22 4 20\n-26 -18 15', 0, Feedback.INCORRECT_COMM_ONE_WRONG_EL),
    ('', 0, Feedback.SYNTAX_ERROR),
]

is_passed = True
for test_num, (reply, test_score, test_feedback) in enumerate(TEST_CASES, start=1):
    score, feedback = check(reply)
    if score != test_score or feedback != test_feedback:
        print(f'Test {test_num} FAILED: {score} != {test_score}')
        is_passed = False

if is_passed:
    print('All tests PASSED')
