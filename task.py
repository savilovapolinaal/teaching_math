"""
Вычислите значение матричного выражения
$$
\begin{pmatrix} 3 & 2 & 1\\ -1 & 0 & 1\\ -2 & -2 & 0 \end{pmatrix}
*
\begin{pmatrix} 2 & 4 & 1\\ -3 & 0 & 7\\ -1 & -3 & 2 \end{pmatrix}
*
\begin{pmatrix} 3 & 2 & 1\\ -1 & 0 & 1\\ -2 & -2 & 0 \end{pmatrix}
^T
$$
Формат ввода:
Матрица
$$
\begin{pmatrix} 1 & 2 & 3\\ 4 & 5 & 6\\ 7 & 8 & 9 \end{pmatrix}
$$
должна быть записана в следующем виде \[ [1\: 2\: 3; 4\: 5\: 6; 7\: 8\: 9]\]
"""

CORRECT_ANSWER_STR = "34 20 -16\n-22 4 20\n-26 -18 12"
CORRECT_ANSWER = [[34, 20, -16], [-22, 4, 20], [-26, -18, 12]]
INCORRECT_ANSWER_WITHOUT_TR = [[-50, -40, 8], [-4, -8, -10], [46, 36, -6]]
INCORRECT_ANSWER_INCORRECT_TR_0 = [[25, 40, -7], [-15, 8, 13], [-18, -36, 4]]
INCORRECT_ANSWER_INCORRECT_TR_1 = [[44, 21, -17], [-14, 7, 17], [-34, -20, 14]]


class Feedback:
    SYNTAX_ERROR = 'Ошибка. Ответ предоставлен в некорректном формате.'
    CORRECT = 'Верно!'
    INCORRECT_COMM_WITHOUT_TR = 'Неверно. Необходимо выполнить транспонирование.'
    INCORRECT_COMM_INCORRECT_TR = 'Неверно. Транспонирование выполнено некорректно.'
    INCORRECT_COMM_WRONG_DIM = 'Неверно. Порядок итоговой матрицы должен быть равен трем.'
    INCORRECT_COMM_ONE_WRONG_EL = 'Неверно. Вы ошиблись лишь в одном из элементов итоговой матрицы, попробуйте найти ошибку.'
    INCORRECT_COMM_TWO_WRONG_EL = 'Неверно. Вы ошиблись в расчетах двух элементов итоговой матрицы, попробуйте найти ошибки.'
    INCORRECT_COMM_OTHER = 'Неверно.Пропробуйте еще раз.'

##############################


def solve():
    return CORRECT_ANSWER_STR


def parse(reply):
    if reply == '0':
        return '0'
    try:
        matrix = [[float(j) for j in i] for i in [i.split(' ') for i in reply.split('\n')]]
    except ValueError:
        return None
    return matrix

def count_wrong_el(answer, corr_answ):
    count = 0
    for i in range(len(corr_answ)):
        for j in range(len(corr_answ[i])):
            if answer[i][j] != corr_answ[i][j]:
                count = count + 1
    return count

def check(reply):
    answer = parse(reply)
    if answer is None:
        return 0, Feedback.SYNTAX_ERROR

    if answer == CORRECT_ANSWER:
        return 1, Feedback.CORRECT

    if answer == INCORRECT_ANSWER_WITHOUT_TR:
        return 0, Feedback.INCORRECT_COMM_WITHOUT_TR
    if answer == INCORRECT_ANSWER_INCORRECT_TR_0:
        return 0, Feedback.INCORRECT_COMM_INCORRECT_TR
    if answer == INCORRECT_ANSWER_INCORRECT_TR_1:
        return 0, Feedback.INCORRECT_COMM_INCORRECT_TR
    if (len(answer[0]) == 3 & len(answer[1]) == 3 & len(answer[2]) == 3 & len(answer) == 3):
        count = count_wrong_el(answer, CORRECT_ANSWER)
        if count == 1:
            return 0, Feedback.INCORRECT_COMM_ONE_WRONG_EL
        if count == 2:
            return 0, Feedback.INCORRECT_COMM_TWO_WRONG_EL
        return 0, Feedback.INCORRECT_COMM_OTHER
    return 0, Feedback.INCORRECT_COMM_WRONG_DIM
